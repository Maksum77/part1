import React from 'react';
import Task from "../Task/Task";
class TaskList extends React.Component{
    constructor(props){
        super();
        this.state = {}
        this.tasks = props.tasks;
    }
    render(){
        return (
            <ul>
                {this.tasks.map(function(item, i, array) {
                    return <Task name={item} key={i}/>
                })}
            </ul>
        )
    }
}
export default TaskList