import React from 'react';
import ReactDOM from 'react-dom';
import TaskList from './TaskList/TaskList'
import Task from './Task/Task'
const tasks = ['Сходить в магазин', 'Помыть посуду', 'Удариться мизинцем об угол шкафа','Получить ИНН'];
const root = document.getElementById('root');
ReactDOM.render(
    <div style={{fontSize: '30px'}}>
        <h3>Добавить задачу:</h3>
        <input id='create' />
        <button onClick={CreateNewTask}>Добавить</button>
        <h3> Список задач:</h3>
        <TaskList tasks={tasks}/>
    </div>,
    root
);
function CreateNewTask() {
    const input = document.getElementById('create');
   tasks.push(input.value);
    console.log(tasks);
}