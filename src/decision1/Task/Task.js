import React from 'react';

class Task extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            isCompleted: false,
        };
        this.name = props.name;
    }
    Finish = () => {
        this.setState((prevState) => {
            return {
                isCompleted: !prevState.isCompleted
            }
        });
    };
    HandleChildUnmount = () => {

    };
    render() {
        return (
            <li>
                <span>{this.name}</span>
                <button onClick={this.Finish}>{this.state.isCompleted ?  'Возобновить' :'Выполнить'}</button>
                <button onClick={this.HandleChildUnmount}>Удалить</button>
            </li>
            )
    }
}
export default Task;